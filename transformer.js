#! /usr/bin/env node

const modelName = 'consumer';

const model = require('./' + modelName + '.js').schema;

let properties = [];

handleRoot(model, properties);

const data = {
  "type": "object",
  "originalName": modelName,
  "children": properties
};

fs = require('fs');
fs.writeFile('newData.json', JSON.stringify(data), function(err) {
  if (err) return console.log(err);
  console.log('file writen');
});

return data;

////////////////////////////////////////////////////////

function handleProperty(key, value, data, required) {
  let dataElement = (JSON.parse(JSON.stringify(value)));
  dataElement.name = key;
  if (required)
    dataElement.required = true;
  data.push(dataElement);
}

function handleObject(key, value, data) {
  //prepare new element (for data)
  let dataElement = {
    "name": key,
    "type": "object",
    "children": []
  };

  //no object properties or no array type
  //so the element is an object without defined properties
  if (value.properties === undefined && value.items === undefined) {
    dataElement.children = undefined;
    data.push(dataElement);
    return;
  }

  const required = value.required;
  if (required)
    dataElement.required = true;

  //loop through the properties
  for (var key_ in value.properties) {
    const type = value.properties[key_].type;

    if (type === 'object') {
      handleObject(key_, value.properties[key_], dataElement.children);
    }
    else if (type === 'array') {
      //the ajv format needs a nesting
      let element = {
        "name": key_,
        "type": "array",
        "children": []
      };

      if (required)
        element.required = true;

      let items = value.properties[key_].items;

      //here could be a filter
      if (items.type === undefined) {
        const selector = 'oneOf';
        if (!items[selector])
          selector = 'anyOf';
        items = items[selector][0];
        //TODO handle all selectors
      }

      //normally it is an object or an property
      if (items.type === 'object')
        handleObject('', items, element.children);
      else {
        let isRequired = false;
        if (required) {
          isRequired = required.indexOf(key_) !== -1;
        }
        handleProperty('', items, element.children, required)
      }

      dataElement.children.push(element);
    }
    else {
      let isRequired = false;
      if (required) {
        isRequired = required.indexOf(key_) !== -1;
      }
      handleProperty(key_, value.properties[key_], dataElement.children, isRequired);
    }
  }

  data.push(dataElement);
}

function handleRoot(model, targetArray) {
  const required = model.required;
  for (var key_ in model.properties) {
    const type = model.properties[key_].type;
    if (type === 'object') {
      handleObject(key_, model.properties[key_], targetArray);
    } else if (type === 'array') {
      let element = {
        "name": key_,
        "type": "array",
        "children": []
      };

      if (required)
        element.required = true;

      let items = model.properties[key_].items;

      if (items.type === undefined) {
        const selector = 'oneOf';
        items = items[selector][0];
      }

      if (items.type === 'object')
        handleObject('', items, element.children);
      else {
        let isRequired = false;
        if (required) {
          isRequired = required.indexOf(key_) !== -1;
        }
        handleProperty('', items, element.children, required)
      }

      targetArray.push(element);
    } else {
      let isRequired = false;
      if (required) {
        isRequired = required.indexOf(key_) !== -1;
      }
      handleProperty(key_, model.properties[key_], targetArray, isRequired);
    }
  }
}
